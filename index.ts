﻿import { LINQ } from "berish-linq";

export interface IKeyValuePair<TKey, TValue> {
    key: TKey;
    value: TValue;
}
export class KeyValuePair<TKey, TValue> implements IKeyValuePair<TKey, TValue>{
    public key: TKey;
    public value: TValue;
    constructor(key: TKey, value: TValue) {
        this.key = key;
        this.value = value;
    }
}

export class Dictionary<TKey, TValue>{
    private items: KeyValuePair<TKey, TValue>[] = [];

    constructor();
    constructor(...values: KeyValuePair<TKey, TValue>[]);
    constructor(...values: KeyValuePair<TKey, TValue>[]) {
        this.items = values;
    }

    public add(key: IKeyValuePair<TKey, TValue>): void;
    public add(key: TKey, value: TValue): void;
    public add(key: TKey | IKeyValuePair<TKey, TValue>, value?: TValue): void {
        if (value != null)
            this.items.push(new KeyValuePair(key as TKey, value));
        else
            this.items.push(key as IKeyValuePair<TKey, TValue>);
    }

    public containsKey(key: TKey): boolean {
        return this.toLinq().count(m => m.key === key) !== 0;
    }

    public count(): number {
        return this.items.length;
    }

    public get(key: TKey): TValue {
        return this.toLinq().single(m => m.key === key).value;
    }

    public getByValue(value: TValue): TKey[] {
        return this.toLinq().where(m => m.value === value).select(m => m.key).toArray();
    }

    public remove(key: TKey): TValue {
        let kv = this.toLinq().single(m => m.key === key);
        this.items = this.toLinq().where(m => m !== kv).toArray();
        return kv.value;
    }

    public keys(): LINQ<TKey> {
        return this.toLinq().select(m => m.key);
    }

    public values(): LINQ<TValue> {
        return this.toLinq().select(m => m.value);
    }

    public asArray(): KeyValuePair<TKey, TValue>[] {
        return this.items;
    }

    public toLinq(): LINQ<KeyValuePair<TKey, TValue>> {
        return LINQ.fromArray(this.items);
    }

    public toJson(): string {
        return JSON.stringify(this.items);
    }

    public fromJson(value: string): Dictionary<TKey, TValue> {
        return Dictionary.fromJson<TKey, TValue>(value);
    }

    public fromArray(array: IKeyValuePair<TKey, TValue>[]): Dictionary<TKey, TValue> {
        return Dictionary.fromArray(array);
    }

    public fromDictionary(dict: Dictionary<TKey, TValue>): Dictionary<TKey, TValue> {
        return Dictionary.fromDictionary(dict);
    }

    public static fromJson<TKey, TValue>(value: string): Dictionary<TKey, TValue> {
        return new Dictionary<TKey, TValue>(...(JSON.parse(value).items as KeyValuePair<TKey, TValue>[]));
    }

    public static fromArray<TKey, TValue>(array: IKeyValuePair<TKey, TValue>[]): Dictionary<TKey, TValue> {
        return new Dictionary<TKey, TValue>(...array);
    }

    public static fromDictionary<TKey, TValue>(dict: Dictionary<TKey, TValue>): Dictionary<TKey, TValue> {
        return new Dictionary<TKey, TValue>(...dict.items);
    }

}

//export class Dictionary<TKey, TValue> implements IDictionary<TKey, TValue> {
//    private items: { [index: string]: TValue } = {};
//    private count: number = 0;

//    constructor();
//    constructor(...values: KeyValuePair<TKey, TValue>[]);
//    constructor(...values: KeyValuePair<TKey, TValue>[]) {
//        for (let val of values) {
//            this.Add(val.key, val.value);
//        }
//    }

//    public ContainsKey(key: TKey): boolean {
//        return this.items.hasOwnProperty(JSON.stringify(key));
//    }

//    public Count(): number {
//        return this.count;
//    }

//    public Add(key: TKey, value: TValue) {
//        this.items[JSON.stringify(key)] = value;
//        this.count++;
//    }

//    public Remove(key: TKey): TValue {
//        let k = JSON.stringify(key);
//        var val = this.items[k];
//        delete this.items[k];
//        this.count--;
//        return val;
//    }

//    public Get(key: TKey): TValue {
//        return this.items[JSON.stringify(key)];
//    }

//    public GetByValue(value: TValue): TKey[] {
//        let keySet: TKey[] = [];
//        for (let key in this.items) {
//            let val = this.items[key];
//            if (val === value)
//                keySet.push(<TKey>JSON.parse(key));
//        }
//        return keySet;
//    }

//    public Keys(): TKey[] {
//        var keySet: TKey[] = [];
//        for (var prop in this.items) {
//            if (this.items.hasOwnProperty(prop)) {
//                keySet.push(<TKey>JSON.parse(prop));
//            }
//        }
//        return keySet;
//    }

//    public Values(): TValue[] {
//        var values: TValue[] = [];
//        for (var prop in this.items) {
//            if (this.items.hasOwnProperty(prop)) {
//                values.push(this.items[prop]);
//            }
//        }
//        return values;
//    }

//    public AsArray(): Array<IKeyValuePair<TKey, TValue>> {
//        let array: Array<IKeyValuePair<TKey, TValue>> = new Array<IKeyValuePair<TKey, TValue>>();
//        for (var prop in this.items) {
//            if (this.items.hasOwnProperty(prop)) {
//                array.push(new KeyValuePair<TKey, TValue>(<TKey>JSON.parse(prop), this.items[prop]));
//            }
//        }
//        return array;
//    }

//    public FromArray(array: IKeyValuePair<TKey, TValue>[]) {
//        for (let prop of array) {
//            this.Add(prop.key, prop.value);
//        }

//        return this;
//    }
//}